import datetime 
from datetime import timedelta
import airflow
import requests
import pymongo
from pymongo import MongoClient
from airflow import DAG

profil = requests.api.get("https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=a48ba8cae498a3d739b3ad02123cc061")
rating = requests.api.get("https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=a48ba8cae498a3d739b3ad02123cc061")

data = {
  "rating": profil,
  "profil": rating,
  "timestamp":  datetime.datetime.now().timestamp()
}


print("--------------PROFIL-----------------")
print (data['profil'].text)
print("--------------RATING-----------------")
print (data['rating'].text)
print("--------------Timestamp-----------------")
print (time.strftime('%A, %Y-%m-%d %H:%M:%S', time.localtime(data['timestamp'])))



myclient = MongoClient("mongodb://root:root@localhost:27017/")
mydb = myclient["admin"]
mycol = mydb["admin"]

x = mycol.insert_one(data)

print(x.inserted_id)


args = {
    'id': '15',
    'owner': 'Alexis',
    'start_date': datetime(2020, 2, 26)
}

dag = DAG(
    dag_id = '50',
    default_args = args,
    schedule_interval = timedelta(minutes = 1),
    catchup = False
)

